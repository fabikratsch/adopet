/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.alura.enums;

import java.io.Serializable;
import java.util.Arrays;

/**
 *
 * @author Fabi
 */
public enum EnumOpcoesMenu implements Serializable {
    NENHUMA_OPCAO_SELECIONADA(0),
    LISTAR_ABRIGOS(1), CADASTRAR_ABRIGO(2), 
    LISTAR_PETS_DO_ABRIGO(3), IMPORTAR_PETS_DO_ABRIGO(4),
    SAIR(5);

    private final int opcao;

    private EnumOpcoesMenu(int opcao) {
        this.opcao = opcao;
    }
    
    public static EnumOpcoesMenu findByOpcao(int opcao){
        return Arrays.stream(EnumOpcoesMenu.values())
                .filter(e -> e.getOpcao() == opcao)
                .findFirst().orElse(NENHUMA_OPCAO_SELECIONADA);
    }

    public int getOpcao() {
        return opcao;
    }
}
