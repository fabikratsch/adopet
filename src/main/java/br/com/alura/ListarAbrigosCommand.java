/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.alura;

import br.com.alura.client.ClientHttpConfiguration;
import br.com.alura.service.AbrigoService;
import com.google.gson.JsonSyntaxException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fabi
 */
public class ListarAbrigosCommand implements Command {

    @Override
    public void execute() {
        ClientHttpConfiguration client = new ClientHttpConfiguration();
        AbrigoService service = new AbrigoService(client);
        
        try {
            service.listarAbrigosCadastrados();
        } catch (InterruptedException | JsonSyntaxException | IOException ex) {
            Logger.getLogger(ListarAbrigosCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
