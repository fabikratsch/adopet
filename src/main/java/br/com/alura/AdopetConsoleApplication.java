package br.com.alura;

import br.com.alura.enums.EnumOpcoesMenu;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AdopetConsoleApplication {

    public static void main(String[] args) {
        CommandExecutor executor = new CommandExecutor();

        System.out.println("##### BOAS VINDAS AO SISTEMA ADOPET CONSOLE #####");
        try {
            EnumOpcoesMenu opcaoEscolhida = EnumOpcoesMenu.NENHUMA_OPCAO_SELECIONADA;
            while (opcaoEscolhida != EnumOpcoesMenu.SAIR) {
                exibirMenu();

                opcaoEscolhida = retornaNovaOpcaoSelecionada();

                switch (opcaoEscolhida) {
                    case LISTAR_ABRIGOS ->
                        executor.executeCommand(new ListarAbrigosCommand());
                    case CADASTRAR_ABRIGO ->
                        executor.executeCommand(new CadastrarAbrigoCommand());
                    case LISTAR_PETS_DO_ABRIGO ->
                        executor.executeCommand(new ListarPetsPorAbrigoCommand());
                    case IMPORTAR_PETS_DO_ABRIGO ->
                        executor.executeCommand(new ImportarPetsParaAbrigo());
                    default ->
                        System.exit(0);
                }
            }
            System.out.println("Finalizando o programa...");
        } catch (Exception e) {
            Logger.getLogger(AdopetConsoleApplication.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    private static EnumOpcoesMenu retornaNovaOpcaoSelecionada() {
        return EnumOpcoesMenu.findByOpcao(new Scanner(System.in).nextInt());
    }

    private static void exibirMenu() {
        System.out.println("\nDIGITE O NÚMERO DA OPERAÇÃO DESEJADA:");
        System.out.println("1 -> Listar abrigos cadastrados");
        System.out.println("2 -> Cadastrar novo abrigo");
        System.out.println("3 -> Listar pets do abrigo");
        System.out.println("4 -> Importar pets do abrigo");
        System.out.println("5 -> Sair");
    }
}
