/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.alura;

import br.com.alura.client.ClientHttpConfiguration;
import br.com.alura.service.PetService;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fabi
 */
public class ImportarPetsParaAbrigo implements Command {

    @Override
    public void execute() {
        ClientHttpConfiguration client = new ClientHttpConfiguration();
        PetService service = new PetService(client);
        
        try {
            service.importarPetsPorAbrigo();
        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(ImportarPetsParaAbrigo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
