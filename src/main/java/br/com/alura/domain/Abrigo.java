/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.alura.domain;

import java.io.Serializable;

/**
 *
 * @author Fabi
 */
public class Abrigo implements Serializable {

    private long id;
    private String nome;
    private String telefone;
    private String email;
    private Pet[] pets;

    public Abrigo() {
    }

    public Abrigo(String nome, String telefone, String email) {
        this.nome = nome;
        this.telefone = telefone;
        this.email = email;
    }

    public long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public String getEmail() {
        return email;
    }

    public Pet[] getPets() {
        return pets;
    }

    public String retornaJsonDoObjeto() {
        return """
            "id":%s,"nome":"%s","telefone":"%s","email":"%s"
            """.formatted(this.id, this.nome, this.telefone, this.email);
    }

    @Override
    public String toString() {
        return this.id + " - " + this.nome;
    }
}
