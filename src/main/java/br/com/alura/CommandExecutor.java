/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.alura;

import java.io.Serializable;

/**
 *
 * @author Fabi
 */
public class CommandExecutor implements Serializable {

    public void executeCommand(Command command){
        command.execute();
    }
}
